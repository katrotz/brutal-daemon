import ConfigParser
import logging
import MySQLdb
import multiprocessing
import os, sys, time
import datetime
import gzip
import string
#pip install mechanize
import mechanize
#pip install BeautifulSoup
import BeautifulSoup
#pip install phpserialize
import phpserialize
#pip install eyeD3
#import eyed3
#pip install watchdog
import watchdog
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

#Add the library to the include path
sys.path.append(sys.path[0] + '/library/')  

from daemon import Daemon

import os, time, glob, fnmatch





# ======================================================================================================================
#   Encyclopaedia Metallum Daemon
# ======================================================================================================================
class BrutalDaemon(Daemon):

    #Daemon initialization
    def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        #Init parent
        Daemon.__init__(self, pidfile, stdin=stdin, stdout=stdout, stderr=stderr)

        self.configurations     = {}

        self.getConfigurations()

    #Loads the configurations from the file
    def getConfigurations(self,forceReload=False):

        if not len(self.configurations) or forceReload:
            Config      = ConfigParser.ConfigParser()
            Config.read(sys.path[0] + '/config/application.ini')
            sDict       = {}            #Sections
            sections    = Config.sections()
            for section in sections:
                options         = Config.options(section)
                sDict[section]  = ''
                oDict           = {}    #Options
                for option in options:
                    try:
                        oDict[option] = Config.get(section, option)
                        if oDict[option] == -1:
                            logging.debug("skip: %s" % option)
                    except:
                        logging.exception("exception on %s!" % option)
                        oDict[option] = None
                sDict[section]  = oDict
            self.configurations = sDict
        return self.configurations

    def scanfolder(self):
        path = '/Library/WebServer/Documents/metal-archives.com/metall'
        os.chdir(path)
        for name in glob.glob('*'):
            logging.error(name)

    #Actual daemon run
    def run(self):

        #Init
        workersCount    = multiprocessing.cpu_count() * 2

        self.sleepTime  = 5
        self.workers    = []

        #Init logger
        logFilePath     = sys.path[0] + '/logs/log.' + datetime.date.today().strftime("%Y.%m.%d") + '.log'
        logLevel        = self.configurations['logging']['level']
        logging.basicConfig(filename=logFilePath, format='%(asctime)s %(levelname)s %(funcName)s %(lineno)s: %(message)s', level=logLevel)

        logging.info('Num processes: %d' % workersCount)

        #Mark the timestamp the daemon started
        self.startTime  = int(time.time())

        # Establish communication queues
        self.tasks      = multiprocessing.JoinableQueue()
        self.results    = multiprocessing.Queue()

        #Audit and control flags
        self.ac         = AC(self.configurations)

        #Watchdog
        tmpDirectory        = self.configurations['daemon']['tempmusicpath']
        approvedDirectory   = self.configurations['daemon']['localmusicpath']

        self.tmpDirObserver = Observer()
        tmpDirHandler       = TmpDirChangeHandler(self.configurations)
        self.tmpDirObserver.schedule(tmpDirHandler, path=tmpDirectory, recursive=False)
        self.tmpDirObserver.start()

        while True:
            try:
                logging.debug('Daemon cyclic iteration')

                #Audit and control listener will take care of AC tasks
                self.tasks  = self.ac.listen(self.tasks)

                #self.scanfolder()

                # Spawn the workers
                self.spawnWorkers(workersCount)

                #Create the Tasks
                #for i in xrange(workersCount):
                #    self.tasks.put(Task())

                #Add the poison pills
                for i in xrange(workersCount):
                    self.tasks.put(None)

                # Wait for tasks to complete
                self.tasks.join()

                # Process the task results (UPDATE the connection records)
                #for i in xrange(workersCount):
                #    workerResult = self.results.get()

                # Shut down the workers
                self.stopWorkers()
                time.sleep(self.sleepTime)
            except:
                exc_type, exc_obj, exc_tb   = sys.exc_info()
                fname                       = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                logging.error(sys.exc_info())
                logging.error(exc_type)
                logging.error(fname)
                logging.error(exc_tb.tb_lineno)

                #Stop the watchdog
                self.tmpDirObserver.stop()



    def stop(self):
        #Stop the temp directory watchdog
        #self.tmpDirObserver.stop()

        Daemon.stop(self)

    # ==================================================================================================================
    #   Spawns the workers to perform the tasks
    # ==================================================================================================================
    def spawnWorkers(self,workersCount):
        # Start workers
        self.workers = [ Worker(self.tasks, self.results)
                         for i in xrange(workersCount) ]
        for w in self.workers:
            logging.debug('Spawning the worker %s' % w.name)
            time.sleep(0.1)
            w.start()

    # ==================================================================================================================
    #   Stop the workers
    # ==================================================================================================================
    def stopWorkers(self):
        for w in self.workers:
            w.stop()
        self.workers    = []


# ======================================================================================================================
#   Audit And Control
# ======================================================================================================================
class AC(object):
    def __init__(self,configs):
        self.configuration          = configs

    def __call__(self):
        return self

    #AC listener
    def listen(self,tasks):
        #Entry point for encyclopaedia update
        #if self.isEncyclopaediaOutdated():
        #    tasks.put(TaskEncyclopaediaUpdate(self))

        #Fetch the ready tasks to be processed
        newTaskRows    = self.getNewTasks()

        for row in newTaskRows:
            try:
                params  = phpserialize.loads(row['params'])

                tasks.put(row['cur_pgm_name'](params=params))
            except:
                logging.error(sys.exc_info())
                logging.error('Failed to create an AC task for %s' % row['cur_pgm_name'])

        return tasks

    #Checks if the encyclopaedia database is outdated
    def isEncyclopaediaOutdated(self):
        isOutdated  = True

        with DatabaseConnection(self.configuration['db']) as connection:
            try:
                cursor  = connection.getDb().cursor()
                cursor.execute(
                    """
                    SELECT * FROM ac_tasks
                    WHERE cur_pgm_name = "EM_UPDATE"
                    AND status IN ("CO","RD","IU")
                    ORDER BY update_date DESC
                    """
                )

                acRow           = connection.fetchone(cursor)

                logging.debug('Checking the Encyclopaedia Metallum state:')

                if acRow is not None:
                    if acRow['update_date'] == None:
                        raise Exception('Last EM update date not set')

                    lastUpdateTime  = acRow['update_date']
                    lastUpdateLimit = datetime.datetime.fromtimestamp(time.time() - int(self.configuration['encyclopaedia']['updateperioddays'])*7*24*60*60 / 1e3)

                    isOutdated  = lastUpdateTime < lastUpdateLimit
                else:
                    logging.debug('No rows found. EM needs to be updated')
                    isOutdated  = True
            except:
                logging.error(sys.exc_value)
                isOutdated  = True

        logging.debug('Encyclopaedia is outdated: %s' % isOutdated)

        return isOutdated

    #Creates a new encyclopaedia update task
    def newEncyclopaediaUpdateTask(self):
        acRow   = {}
        with DatabaseConnection(self.configuration['db']) as connection:
            try:
                cursor  = connection.getDb().cursor()
                sql = """INSERT INTO ac_tasks
                                 (cur_pgm_name, nxt_pgm_name, status)
                                 VALUES
                                 ('EM_UPDATE', 'EM_BANDS_LIST_UPDATE', 'RD')"""
                cursor.execute(sql)
                connection.getDb().commit()

                cursor.execute(
                    """
                    SELECT * FROM ac_tasks
                    WHERE cur_pgm_name = "EM_UPDATE"
                    AND status = "RD"
                    ORDER BY update_date DESC
                    """
                )

                acRow           = connection.fetchone(cursor)
            except:
                logging.error(sys.exc_value)

        return acRow

    #Executes sql over ac
    def executeSql(self,sql):

        with DatabaseConnection(self.configuration['db']) as connection:
            try:
                cursor  = connection.getDb().cursor()
                cursor.execute(sql)
                connection.getDb().commit()
            except:
                logging.error(sys.exc_value)

        return cursor

    def insertTask(self,cur_pgm_name,nxt_pgm_name,prev_pgm_name,params,status='RD',parent_id='NULL'):

        #creation_date   = datetime.datetime.utcnow()
        creation_date   = datetime.datetime.now()

        sql = """INSERT INTO ac_tasks
                 (parent_id, cur_pgm_name, nxt_pgm_name, prev_pgm_name, status, creation_date, params)
                 VALUES
                 (%s, '%s', '%s', '%s', '%s', '%s', '%s')"""\
                % (parent_id,cur_pgm_name,nxt_pgm_name,prev_pgm_name,status,creation_date,params)

        return self.executeSql(sql)

    #Update status
    def updateRowStatus(self,rowID,status):
        with DatabaseConnection(self.configuration['db']) as connection:
            try:
                cursor  = connection.getDb().cursor()

                sql = """UPDATE ac_tasks
                         SET
                             status          = '%s'
                         WHERE
                             id             = '%d'"""\
                        % (status,rowID)
                cursor.execute(sql)
                connection.getDb().commit()
            except:
                logging.error(sys.exc_value)

    def getNewTasks(self):
        newTasks    = {}
        with DatabaseConnection(self.configuration['db']) as connection:
            try:
                cursor  = connection.getDb().cursor()
                cursor.execute("""
                    SELECT * FROM ac_tasks
                    WHERE status = 'RD'
                    ORDER BY creation_date ASC
                """)

                newTasks    = connection.fetchall(cursor)
            except:
                logging.error(sys.exc_value)

        return newTasks


class DatabaseConnection(object):
    def __init__(self,dbConfigs):

        self.configuration  = dbConfigs

    def __enter__(self):
        # make a database connection and return it
        #ACHTUNG! Always use 'use_unicode' when inserting unicode strings into db
        self.db      = MySQLdb.connect(
            self.configuration['host'],
            self.configuration['username'],
            self.configuration['password'],
            self.configuration['dbname'],
            charset='utf8',
            use_unicode=True
        )
        return self

    def getDb(self):
        return self.db

    #Fetch row as dict
    def fetchone(self,cursor):
        record  = None
        try:
            columns         = tuple( [d[0] for d in cursor.description] )
            row             = cursor.fetchone()

            if row is not None:
                record          = dict(zip(columns, row))
        except:
            logging.error(sys.exc_info())
        finally:
            return record

    #Fetch rows as dicts
    def fetchall(self,cursor):
        records = None
        try:
            columns = tuple( [d[0] for d in cursor.description] )
            rows    = cursor.fetchall()
            if rows is not None:
                records = []
                records+= [dict(zip(columns, row)) for row in rows]
        except:
            logging.error(sys.exc_info())
        finally:
            return records

    def __exit__(self, type, value, tb):
        # make sure the dbconnection gets closed
        self.db.close()


# ======================================================================================================================
#   Handles the temporary upload directory changes
# ======================================================================================================================
class TmpDirChangeHandler(FileSystemEventHandler):
    def __init__(self,configs):
        self.configuration          = configs
        self.ac                     = AC(self.configuration)

    def on_created(self, event):

        #The creation detected - give it some time to finish
        time.sleep(5)

        newBandPath = event.src_path

        logging.debug('Addition of a new band to the temp directory detected: "%s"' % newBandPath)

        self.ac.insertTask('ARTIST_VALIDATOR','ARTIST_VALIDATOR','TMP_DIR_WATCHER',phpserialize.dumps({'path':newBandPath}))


    def on_modified(self, event):
        #Handle any kind of modification
        logging.debug('Temporary directory modification detected')

    def on_deleted(self, event):
        logging.debug('Deleted!')

    def on_moved(self, event):
        logging.debug('Moved!')

    def getDirSize(self,path):
        if os.path.isfile(path):
            return os.path.getsize(path)
        else:
            return sum([os.path.getsize(f) for f in os.listdir(path) if os.path.isfile(f)])

# ======================================================================================================================
#   Scrapping Task
# ======================================================================================================================
class TaskEncyclopaediaUpdate(object):
    def __init__(self,ac):
        self.ac         = ac
    def __call__(self):
        acRow = self.ac.newEncyclopaediaUpdateTask()

        #Update AC row
        sql = """UPDATE ac_tasks
                 SET
                     status          = '%s'
                 WHERE
                     id             = '%d'"""\
                % ('IU',acRow['id'])
        self.ac.executeSql(sql)
        #http://www.metal-archives.com/browse/ajax-letter/l/D/json/1?_=1381748965851&sEcho=1&iColumns=4&sColumns=&iDisplayStart=0&iDisplayLength=500&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&mDataProp_3=3&iSortingCols=1&iSortCol_0=0&sSortDir_0=asc&bSortable_0=true&bSortable_1=true&bSortable_2=true&bSortable_3=false

        for char in string.ascii_uppercase:
            url     = 'http://www.metal-archives.com/browse/ajax-letter/l/'+char+'/json/1'
            sql = """INSERT INTO ac_tasks
                                 (parent_id,cur_pgm_name, nxt_pgm_name, status, params)
                                 VALUES
                                 (%d,'EM_BANDS_LIST_UPDATE', 'EM_BAND_INFO_UPDATE', 'RD', '%s')"""\
                                % (acRow['id'],phpserialize.dumps({'url':url}))

            self.ac.executeSql(sql)

        return True


class TaskEncyclopaediaBandsListUpdate(object):
    def __init__(self,ac):
        self.ac         = ac
    def __call__(self):
        acRow = self.ac.newEncyclopaediaUpdateTask()

        #Update AC row
        sql = """UPDATE ac_tasks
                 SET
                     status          = '%s'
                 WHERE
                     id             = '%d'"""\
                % ('IU',acRow['id'])
        self.ac.executeSql(sql)
        #http://www.metal-archives.com/browse/ajax-letter/l/D/json/1?_=1381748965851&sEcho=1&iColumns=4&sColumns=&iDisplayStart=0&iDisplayLength=500&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&mDataProp_3=3&iSortingCols=1&iSortCol_0=0&sSortDir_0=asc&bSortable_0=true&bSortable_1=true&bSortable_2=true&bSortable_3=false

        for char in string.ascii_uppercase:
            url     = 'http://www.metal-archives.com/browse/ajax-letter/l/'+char+'/json/1'
            sql = """INSERT INTO ac_tasks
                                 (parent_id,cur_pgm_name, nxt_pgm_name, status, params)
                                 VALUES
                                 (%d,'EM_BANDS_LIST_UPDATE', 'EM_BAND_INFO_UPDATE', 'RD', '%s')"""\
                                % (acRow['id'],phpserialize.dumps({'url':url}))

            self.ac.executeSql(sql)

        return True


class ARTIST_VALIDATOR(object):
    def __init__(self,params={}):
        self.taskParams = params

    def __call__(self):
        logging.error(self.taskParams)
        return 'Task 2 result'

class Task2(object):
    def __init__(self,type=None,params={}):
        self.taskType   = type
        self.taskParams = params

    def __call__(self):
        time.sleep(0.1)
        logging.info('Task 2 done')
        return 'Task 2 result'

# ======================================================================================================================
#   Worker takes care of the task
# ======================================================================================================================
class Worker(multiprocessing.Process):

    def __init__(self,tasks,results):
        multiprocessing.Process.__init__(self)
        self.tasks  = tasks
        self.results = results

    def __call__(self):
        return self

    def run(self):
        processName = self.name
        while True:
            try:
                #Fetch a new task
                nextTask = self.tasks.get(True)

                #Poison pill
                if nextTask is None:
                    logging.debug('Worker %s encountered empty task queue' % processName)
                    self.tasks.task_done()
                    #time.sleep(3)
                    #continue
                    break

                #Execute the task
                taskResponse = nextTask()

                #Remove the task from the queue
                self.tasks.task_done()

                #Inform the main process about the successfully processed record
                self.results.put(taskResponse)
            except:
                logging.error('Worker %s encountered an error and had to stop' % processName)
                logging.error(sys.exc_info())
                break
        return

    def stop(self):
        logging.debug('Worker %s received stop signal' % self.name)
        multiprocessing.Process.terminate(self)


















#Utility function
def ungzipResponse(r,b):
    headers = r.info()
    try:
        if headers['Content-Encoding']=='gzip':
            gz = gzip.GzipFile(fileobj=r, mode='rb')
            html = gz.read()
            gz.close()
            headers["Content-type"] = "text/html; charset=utf-8"
            r.set_data( html )
            b.set_response(r)
    except KeyError:
        #Ninja
        logging.warning('error')


# ======================================================================================================================
#   Main program run
# ======================================================================================================================
if __name__ == "__main__":

    #Set the process name
    try:
        import setproctitle
        setproctitle.setproctitle('Brutal Daemon')
    except:
        pass

    #Init the daemon
    daemon  = BrutalDaemon('/tmp/brutal_daemon.pid')

    #signal.signal(signal.SIGTERM, signal_handler)

    if len(sys.argv) == 2:

        if 'start' == sys.argv[1]:
            print "Getting brutal\n"
            daemon.start()

        elif 'stop' == sys.argv[1]:
            print "Sending brutal stop signal\n"
            daemon.stop()

        elif 'restart' == sys.argv[1]:
            daemon.stop()
            daemon.start()

        elif 'help' == sys.argv[1]:
            print 'Too brutal to print help messages'

        else:
            print "Unknown command received!"
            print "Usage: python %s start|stop|restart|help" % sys.argv[0]
            sys.exit(2)
        sys.exit(0)
    else:
        print "Usage: python %s start|stop|restart|help" % sys.argv[0]
        sys.exit(2)

